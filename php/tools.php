<?php

//Функция для печати массиа и объектов в преформатированном виде
function pre($data){
	echo "<pre>";
	var_dump($data);
	echo "</pre>";
}

//Функция для получения массива из справочников
function getSprArr(){
	global $DB;
	$result = array();
	$sql = "SELECT k.id k_id,k.name k_name,k.sort,k.active k_active,k.label,v.* 
				FROM spr_key k
				LEFT JOIN spr_val v ON k.id = v.id_spr
				ORDER BY k.active desc,k.sort,v.value desc";
	if($q = $DB->Query($sql)){
		while($r = $q->Fetch(PDO::FETCH_ASSOC)){
			$result[$r["k_id"]]["name"] = $r["k_name"];
			$result[$r["k_id"]]["sort"] = $r["sort"];
			$result[$r["k_id"]]["active"] = $r["k_active"];
			$result[$r["k_id"]]["label"] = $r["label"];
			$result[$r["k_id"]]["list"][$r["id"]] = $r["value"];
			$result[$r["k_id"]]["active_answer"][$r["id"]] = $r["active"];
			$result[$r["k_id"]]["score"][$r["id"]] = $r["score"];
		}
	}
	return $result;
}

//Функция для форматирования даты
function getDateFormat($date,$format='Y-m-d'){
	return ($date>0 ? date($format,strtotime($date)) : "");
}

//Функция для получения максимального бала
function getMaxScore(){
	global $DB;
	$sql = "SELECT MAX(score) score FROM `spr_val` GROUP BY id_spr";
	$result = 0;
	if($q = $DB->Query($sql)){
		while($r = $q->Fetch(PDO::FETCH_ASSOC)){
			$result += floatval($r["score"]);
		}
	}
	return $result;
}

//Функция для подсчёта баллов
function calcScore($scores, $answers){
	$summa = 0;
	foreach($answers as $k=>$v){
		$summa += intval($scores[$k]["score"][$v]);
	}
	return $summa;
}

//Функция для подсчета интервала между двумя датами
function dateDifference($date_1 , $date_2 , $differenceFormat = '%y' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
   
    $interval = date_diff($datetime1, $datetime2);
   
    return $interval->format($differenceFormat);
   
}

//Автоматическое определение балла для возраста клиента
function getAnswerIdByRange($id_spr,$val){
	global $DB;
	$res = array("id"=>0);
	$sql = "SELECT id FROM `spr_val` WHERE id_spr = {$id_spr} and max_val>={$val} ORDER BY min_val LIMIT 1";
	if($q = $DB->Query($sql)){
		$res = $q->Fetch(PDO::FETCH_ASSOC);
	}
	return $res["id"];
}

//Генерация формы из справочников
function getForm($spr,$id_client){
	$client = getClientList($id_client);
	$age = dateDifference(date("Y-m-d"),$client["date_born"]);
	$result = '<div class="form-group">
					<label>ФИО</label>
					<input name="info[ФИО]" class="form-control" value="'.$client["fio"].'">
				</div>
				<div class="form-group">
					<label>Телефон</label>
					<input name="info[Телефон]" class="form-control" value="'.implode(", ",$client["phones"]).'">
				</div>';

	foreach($spr as $k=>$v){
		$sel = ($v["label"] == "age" ? getAnswerIdByRange($k,$age) : 0);
		$result .= ($v["active"]>0 ? '<div class="form-group">
					    <label>'.$v["name"].'</label>
					    <select name="answer['.$k.']" class="form-control">'.getOptions($v["list"],$sel).'</select>
					</div>' : '');
	}
	return $result;
}


//Вспомогательная функция для вставки в БД
function insert($tbl,$data){
	global $DB;
	$fld = $val = "";
	if(is_assoc($data)){ // Ассоциативный одномерный массив для однострочной вставки
		foreach($data as $k=>$v){
			$fld .= $k.",";
			if(is_array($v)){
				$v = implode(",",$v);
				$val .= "',".$v.",',";
			}else{
				$val .= "'".$v."',";
			}
		}
		$fld = substr($fld,0,-1);
		$val = substr($val,0,-1);
		$sql = "INSERT INTO ".$tbl."($fld) VALUES($val)"; //echo $sql."<br>";
	}else{				// Многомерный массив для многострочной вставки
		foreach($data as $k=>$v){
			$fld = "";
			$val .= "(";
			foreach($v as $kk=>$vv){
				$fld .= $kk.",";
				if(is_array($vv)){
					$vv = implode(",",$vv);
				}
				$val .= "'".$vv."',";
			}
			$val = substr($val,0,-1);
			$val .= "),";
			
		}
		$fld = substr($fld,0,-1);
		$val = substr($val,0,-1);
		$sql = "INSERT INTO ".$tbl."($fld) VALUES$val";
	}
	$sql = cleanSQL($sql);
	$DB->Query($sql);
	return $DB->lastInsertId();
}
//Вспомогательная функция для обновления БД
function update($tbl,$data,$key = "id"){
	global $DB;
	if(is_assoc($data)){ // Ассоциативный одномерный массив для однострочной вставки
		$sql = "UPDATE ".$tbl." SET ";
		$cond = "";
		foreach($data as $k=>$v){
			if($k == $key){
				$cond = " WHERE ".$k." = ".$v;
			}else{
				$sql .= $k." = '".$v."',";
			}
		}
		$sql = substr($sql,0,-1);
		$sql .= $cond;echo $sql."<br>";
		$sql = cleanSQL($sql);
		$DB->Query($sql); 
		return 1;
	}else{							// Многомерный массив для многострочного обновления
		foreach($data as $k=>$v){
			$sql = "UPDATE ".$tbl." SET ";
			$cond = "";
			foreach($v as $kk=>$vv){
				if($kk == $key){
					$cond = " WHERE ".$kk." = ".$vv;
				}else{
					if($kk=="sikl_ispolzovaniya"){
						$sql .= $kk." = ".$vv.",";
					}else{
						$sql .= $kk." = '".$vv."',";
					}
				}				
			}
			$sql = substr($sql,0,-1);
			$sql .= $cond;
			$sql = cleanSQL($sql);
			$DB->Query($sql); echo $sql."<br>";
		}
		return 1;
	}
}
//Вспомогательная функция для удаления из БД
function delete($tbl,$data,$key="id"){
	global $DB;
	$ids = implode(",",$data);
	$sql = "DELETE FROM ".$tbl." WHERE ".$key." in('".$ids."')";
	return $DB->Query($sql);
}
//Вспомогательная функция для определения типа массива
function is_assoc($array) {
  return (bool)count(array_filter(array_keys($array), 'is_string'));
}

//Вспомогательная функция для очистки SQL
function cleanSQL($sql){
	$str = str_replace("'","\'",$sql);
	$str = str_replace('"','\"',$sql);
	return $str;
}

//Получение списка клиентов
function getClientList($id=0,$inn=0,$list=0){
	global $DB;
	$result = array();
	$sql = "SELECT c.*,ci.name city_name,p.phone,p.id pid
			FROM clients c
			LEFT JOIN city ci ON ci.id=c.id_city
			LEFT JOIN phones p ON p.id_client=c.id";
	$sql .= ($id>0 ? " WHERE c.id=".$id : "");
	$sql .= ($inn>0 ? " WHERE c.inn=".$inn : "");
	
	if($q = $DB->Query($sql)){
		if($list>0){
			while($r = $q->Fetch(PDO::FETCH_ASSOC)){
				$result[$r["id"]] = $r["fio"]." ".$r["inn"];
			}
		}else{
			while($r = $q->Fetch(PDO::FETCH_ASSOC)){
				$result[$r["id"]]["id"] = $r["id"];
				$result[$r["id"]]["fio"] = $r["fio"];
				$result[$r["id"]]["inn"] = $r["inn"];
				$result[$r["id"]]["date_born"] = $r["date_born"];
				$result[$r["id"]]["id_city"] = $r["id_city"];
				$result[$r["id"]]["sex"] = $r["sex"];
				$result[$r["id"]]["city_name"] = $r["city_name"];
				$result[$r["id"]]["phones"][$r["pid"]] = $r["phone"];
			}
		}
	}
	$result = ($id>0 ? $result[$id] : $result);
	return $result;
}

//Список городов
function getCityList($id=0){
	global $DB;
	$result = array();
	$sql = "SELECT * FROM city";
	
	if($q = $DB->Query($sql)){
		while($r = $q->Fetch(PDO::FETCH_ASSOC)){
			$result[$r["id"]] = $r["name"];
		}
	}
	return $result;
}

//Список результатов
function getResultList($id=0, $limit=0){
	global $DB;
	$result = array();
	$sql = "SELECT r.*,c.id cid,c.fio,c.inn
			FROM result r 
			LEFT JOIN clients c ON c.id=r.`id_client`
			WHERE 1=1";
	$sql .= ($id>0 ? " and id=".$id : "");
	$sql .= ($limit>0 ? " order by id desc limit ".$limit : "");
	
	if($q = $DB->Query($sql)){
		while($r = $q->Fetch(PDO::FETCH_ASSOC)){
			$result[$r["id"]] = $r;
		}
	}
	return $result;
}

//Получение списка для элемента select
function getOptions($arr,$ids=0){
	$res = '';
	if(is_array($arr)){
		if(is_array($ids)){
			foreach ($arr as $k=>$v){
				$sel = (array_key_exists($k,$ids) ? 'selected' : '');
				$res .= '<option value="'.$k.'" '.$sel.'>'.$v.'</option>';
			}
		}else{
			foreach ($arr as $k=>$v){
				$sel = ($k == $ids ? 'selected' : '');
				$res .= '<option value="'.$k.'" '.$sel.'>'.$v.'</option>';
			}
		}
	}
	return $res;
}

//Функция для печати excel
function toExcel($type="result"){
	 require_once 'lib/PHPExcel/Classes/PHPExcel/IOFactory.php';

	$InputFileName = "template/".$type.'.xlsx';
	$objReader = PHPExcel_IOFactory::createReader('Excel2007'); 
	//$objReader->setIncludeCharts(TRUE); 
	$objPHPExcel = $objReader->load($InputFileName); 
    $objPHPExcel->setActiveSheetIndex(0);
    $aSheet = $objPHPExcel->getActiveSheet();

	$i = 2;
	$list = ($type=="result" ? getResultList() :  getClientList());
	
	$styleArray = array(
	  'borders' => array(
		'allborders' => array(
		  'style' => PHPExcel_Style_Border::BORDER_THIN
		)
	  )
	);

	if($type=="result"){
	    foreach ($list as $row) {
			$aSheet->setCellValue('A' . $i, $row['cid']);
			$aSheet->setCellValue('B' . $i, $row['fio']);
			$aSheet->setCellValue('C' . $i, $row['inn']);
			$aSheet->setCellValue('D' . $i, $row['percent']);
			$aSheet->setCellValue('E' . $i, getDateFormat($row['date_create'],'d.m.Y'));
			$i++;
		}
		$aSheet->getStyle('A1:E'.($i-1))->applyFromArray($styleArray);
	}else{
    	foreach ($list as $row) {
			$aSheet->setCellValue('A' . $i, $row['id']);
			$aSheet->setCellValue('B' . $i, $row['fio']);
			$aSheet->setCellValue('C' . $i, $row['inn']);
			$aSheet->setCellValue('D' . $i, getDateFormat($row['date_born'],'d.m.Y'));
			$aSheet->setCellValue('E' . $i, $sex[$row['sex']]);
			$aSheet->setCellValue('F' . $i, $row['city_name']);
			$i++;
	    }
	    $aSheet->getStyle('A1:F'.($i-1))->applyFromArray($styleArray);
	}
	
	
	
    //PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->setIncludeCharts(TRUE);
	
    // Redirect output to a client’s web browser (Excel2007)
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel.sheet.macroEnabled.12');
    header('Content-Disposition: attachment;filename="'.$type.date('d.m.Y').'.xlsx"');
    header('Cache-Control: max-age=0');
    $objWriter->save('php://output');
    exit;
}

//Определение корня сайта
define("SITE_DIR", "/scoring");

//Массив элементов меню
$menu = array(
	0 => array("name"=>"Статистика","link"=>SITE_DIR),
	1 => array("name"=>"Скоринг","link"=>SITE_DIR."?c=scoring"),
	2 => array("name"=>"Мизочон","link"=>SITE_DIR."?c=client"),
	3 => array("name"=>"Таърих","link"=>SITE_DIR."?c=history"),
	4 => array("name"=>"Чурсози","link"=>SITE_DIR."?c=config"),
);

//Массив пол клиента
$sex = array(
	0 => "Мард",
	1 => "Зан"
);