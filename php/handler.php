<?php
include("conn.php");
include("tools.php");


if(isset($_POST["editSPR"])){
	$arr = array();
	foreach($_POST["active"] as $k=>$v){
		$arr[] = array(
			"name" => $_POST["name"][$k],
			"active" => $v,
			"sort" => $_POST["sort"][$k],
			"id" => $k
		);
	}
	update("spr_key",$arr);
	header("Location:../index.php?c=config");
}

if(isset($_POST["editSPRById"])){
	$arr = array();
	$id = intval($_POST["curSpr"]);
	foreach($_POST["active"] as $k=>$v){
		$arr[] = array(
			"id_spr" => $_POST["id_spr"][$k],
			"value" => $_POST["value"][$k],
			"score" => $_POST["score"][$k],
			"active" => $v,
			"id" => $k
		);
	}
	//pre($arr);exit;
	update("spr_val",$arr);
	
	$new = array();
	if(isset($_POST["new_answer"]) && $id>0){
		foreach($_POST["new_answer"] as $k=>$v){
			$score = intval($_POST["new_score"][$k]);
			if(strlen($v)<1 || $score<1){
				continue;
			}
			$new[] = array(
				"id_spr" => $id,
				"value" => $v,
				"score" => $score
			);
		}
		insert("spr_val",$new);
	}

	header("Location:../index.php?c=config&spr=".intval($_POST["curSpr"]));
}

if(isset($_POST["newQuestion"])){
	$question = array(
		"name" => $_POST["question"]
	);
	$id = insert("spr_key",$question);
	$answers = array();
	pre($_POST);exit;
	if(isset($_POST["new_answer"]) && $id>0){
		foreach($_POST["new_answer"] as $k=>$v){
			$score = intval($_POST["new_score"][$k]);
			if(strlen($v)<1 || $score<1){
				continue;
			}
			$answers[] = array(
				"id_spr" => $id,
				"value" => $v,
				"score" => $score
			);
		}
		pre($answers);exit;
		insert("spr_val",$answers);
		header("Location:../index.php?c=config&spr=".$id);	
	}
	header("Location:../index.php?c=config");	
}


if(isset($_POST["edit_client"])){
	$id = intval($_POST["id"]);
	$client = array(
		"fio" => $_POST["fio"],
		"inn" => $_POST["inn"],
		"date_born" =>getDateFormat($_POST["date_born"]),
		"sex" => $_POST["sex"],
		"id_city" => $_POST["city"]
	);

	if($id>0){
		$client["id"] = $id;
		update("clients",$client);
	}else{
		$old = getClientList(0,$_POST["inn"]);
		if(count($old)>0){
			header("Location:".SITE_DIR."/?c=edit_client&inn_error=1");
		}
		insert("clients",$client);
	}
	header("Location:".SITE_DIR."/?c=client");
}

if(isset($_POST["calc"])){
	$id = intval($_POST["id_client"]);
	if($id>0){
		$client = getClientList($id);
		$spr = getSprArr();
	    $answers = $_POST["answer"];
	    $score = calcScore($spr, $answers);
	    $maxScore = getMaxScore();
		$percentage = round($score/$maxScore*100,2);
	    $resultScore = $percentage*0.05;
	}
}