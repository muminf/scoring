
  <h1 class="m-0 text-dark">Результат скоринга</h1>
          </div>
        </div>
      </div>
    </div>
<?php
    $spr = getSprArr();
    $answers = $_POST["answer"];
    $score = calcScore($spr, $answers);
    $maxScore = getMaxScore();
	$percentage = round($score/$maxScore*100,2);
    $resultScore = $percentage*0.05;
    $info = $_POST["info"];
?>
<div class="row">
    <div class="col-lg-12 col-12">
        <div class="small-box bg-warning center-block">
          <div class="inner">
            <?php foreach($info as $k=>$v):?>
                <h4><?=$k?> : <b><?=$v;?></b></h4>
            <?php endforeach;?>
            <h4>Всего баллов: <b><?=$maxScore;?></b></h4>
            <h4>Набрано баллов: <b><?=$score."(".$percentage.")";?></b></h4>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-6">
		
		<div id="container">
            <div id="element"></div>
        </div> 
	</div>
	<div class="col-md-2"></div>
</div>


<script>
    var ele = document.getElementById('container'); 
</script>
<script>
    ej.base.enableRipple(true);
    // initialize circulargauge control
        var config = {
            title: '',
            width: '100%', height: '100%',
            axes: [{
                minimum: 0,
                maximum: 5,
                ranges: [{
                    start: 0,
                    end: 2, 
                    endWidth: 12,
                    startWidth: 12,
                    color: '#EF7E63'
                },
                {
                    start: 2,
                    end: 3.5, 
                    endWidth: 12,
                    startWidth: 12,
                    color: '#E7CC61'
                },
                {
                    start: 3.5,
                    end: 5, 
                    endWidth: 12,
                    startWidth: 12,
                    color: '#8ECB60'
                }],
                pointers: [{
                    value: <?=$resultScore;?>
                }]
            }]
        };
        var circulargauge = new ej.circulargauge.CircularGauge(config);
        circulargauge.appendTo('#element');
</script>