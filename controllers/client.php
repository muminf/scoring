<?php 
	$title = "Мизочон"; 
	include_once("parts/title_header.php");
	$id = (isset($_GET["id"]) ? intval($_GET["id"]) : 0);
	$client_list = getClientList($id);
?>

<a href="<?=SITE_DIR;?>/?c=edit_client"><button class="btn btn-primary">Илова намудан</button></a>
<div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between">
          <h4 class="card-title mb-0">Мизочон</h4>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-hover datatable">
            <thead>
              <tr>
                <th>ID мизоч</th>
                <th>Ному насаби мизоч</th>
                <th>РМА</th>
                <th>Санаи таваллуд</th>
                <th>Чинс</th>
                <th>Шахр</th>
              </tr>
            </thead>
            <tbody>
             <?php foreach($client_list as $k=>$v):?>
              <tr>
                <td><?=$v["id"];?></td>
                <td><a href="?c=edit_client&id=<?=$v["id"];?>"><?=$v["fio"];?></td>
                <td><?=$v["inn"];?></td>
                <td><?=$v["date_born"];?></td>
                <td><?=$sex[$v["sex"]];?></td>
                <td><?=$v["city_name"];?></td>
              </tr>
             <?php endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>