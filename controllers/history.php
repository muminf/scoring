<?php 
$title = "Таърихи хисобот";
include_once("parts/title_header.php");  
  $list = getResultList();
?>
<div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between">
          <h4 class="card-title mb-3">Таърихи хисобот</h4>
        </div>
        <div class="table-responsive">
          <table class="table table-striped table-hover datatable">
            <thead>
              <tr>
                <th>ID мизоч</th>
                <th>Ному насаби мизоч</th>
                <th>РМА</th>
                <th>Натича %</th>
                <th>Санаи хисоб</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($list as $k=>$v):?>
              <tr>
                <td><?=$v["cid"];?></td>
                <td><?=$v["fio"];?></td>
                <td><?=$v["inn"];?></td>
                <td><?=$v["percent"];?></td>
                <td><?=$v["date_create"];?></td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<style>
	.w-100 {
	    width: 65% !important;
	}
</style>