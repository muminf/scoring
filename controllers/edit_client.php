<?php 
if(isset($_GET["id"]) && intval($_GET["id"])>0){
	$id = intval($_GET["id"]);
	$client = getClientList($id);
}
	$cities = getCityList();
	$title = "Мизоч: ".$client["fio"]; 
	include_once("parts/title_header.php");
?>

<div class="col-md-8 grid-margin">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between">
          <h4 class="card-title mb-0">Маълумот дар бораи мизоч</h4>
        </div>
        <div class="table-responsive" style="margin-top:20px">
         <form action="php/handler.php" method="post">
         	<div class="form-group">
         		<label>Ному насаб</label>
         		<input name="fio" class="form-control" value="<?=$client["fio"]?>">
         	</div>
         	<div class="form-group">
         		<label>РМА</label>
         		<input name="inn" class="form-control" value="<?=$client["inn"]?>">
         	</div>
         	<div class="form-group">
         		<label>Санаи таваллуд</label>
         		<input name="date_born" class="form-control datepicker" value="<?=$client["date_born"]?>">
         	</div>
         	<div class="form-group">
         		<label>Чинс</label>
         		<select name="sex" class="form-control"><?=getOptions($sex,$client["sex"]);?></select>
         	</div>
         	<div class="form-group">
         		<label>Шахр</label>
         		<select name="city" class="form-control chosen-select"><?=getOptions($cities,$client["city"]);?></select>
         	</div>
         	<div class="form-group" style="text-align: right">
         		<input type="hidden" name="id" value="<?=$client["id"];?>">
         		<input type="submit" name="edit_client" value="Сабт намудан" class="btn btn-primary">
         		<input type="reset" value="Тоза кардан" class="btn btn-danger">
         	</div>

         </form>
        </div>
      </div>
    </div>
  </div>
</div> 

<style>
	.w-100 {
	    width: 65% !important;
	}
</style>