<?php 
$title = "Скоринг";
include_once("parts/title_header.php");
?>   

<div class="col-md-8 grid-margin" id="main_col_result">
    <div class="card">
      <div class="card-body">
        

      	<?php if(!isset($_GET["id_client"]) && !isset($_GET["result"])):?>
	        <div class="d-flex justify-content-between">
	          <h4 class="card-title mb-2">Мизочро интихоб кунед</h4>
	        </div>
	         <form action="<?=SITE_DIR;?>/?c=scoring">
	         	<div class="form-group">
	         		<select name="id_client" class="form-control chosen-select" data-placeholder='Мизочро интихоб кунед'><option></option>option><?=getOptions($clients_list);?></select>
	         	</div>
	         	<div class="form-group" style="text-align: right">
	         		<input type="hidden" name="c" value="scoring">
	         		<input type="submit" name="calc" value="Интихоб" class="btn btn-primary">
	         	</div>
	         </form>
     	<?php endif;?>

     	<?php if(isset($_GET["id_client"])):
     			$id = intval($_GET["id_client"]);
     		    $form = getForm($spr,$id);
     	?>
	        <div class="d-flex justify-content-between">
	          <h4 class="card-title mb-2">Анкетаро пур кунед</h4>
	        </div>
	         <form action="<?=SITE_DIR;?>/?c=scoring&result=1" method="POST">
			  <?=$form;?>
			  <input type="hidden" name="id_client" value="<?=$id;?>">			    
			  <button type="submit" name="calc" class="btn btn-primary" style="float:right">Хисоб намудан</button>
			</form>
     	<?php endif;?>

     	<?php if(isset($_GET["result"])):
     			$id = intval($_POST["id_client"]);
     		    $client = getClientList($id);
				$spr = getSprArr();
			    $answers = $_POST["answer"];
			    $score = calcScore($spr, $answers);
			    $maxScore = getMaxScore();
				$percentage = round($score/$maxScore*100,2);
			    $resultScore = $percentage*0.05;
			    $result = array("id_client"=>$id,"date_create"=>date("Y-m-d H:i:s"),"max_score"=>$maxScore,"score"=>$score,"percent"=>$percentage);
			    insert("result",$result);
     	?>
     		<style>
     			#main_col_result{

     			}
     		</style>
	        <div class="d-flex justify-content-between">
	          <h2 class="card-title mb-2" style="font-size: 2em">Натичаи скоринг</h2>
	        </div>
	         <div id="container">
	            <div id="element"></div>
	            <h2>Натича <?=$resultScore;?> (<?=$percentage;?>%)</h2>
	        </div> 
	        <script src="assets/js/ej2.min.js" type="text/javascript"></script>
	        <script>
			    var ele = document.getElementById('container');
			    console.log(ele); 
			</script>
			<script>
			    ej.base.enableRipple(true);
			    // initialize circulargauge control
			        var config = {
			            title: '',
			            width: '100%', height: '100%',
			            axes: [{
			                minimum: 0,
			                maximum: 5,
			                ranges: [{
			                    start: 0,
			                    end: 2, 
			                    endWidth: 12,
			                    startWidth: 12,
			                    color: '#EF7E63'
			                },
			                {
			                    start: 2,
			                    end: 3.5, 
			                    endWidth: 12,
			                    startWidth: 12,
			                    color: '#E7CC61'
			                },
			                {
			                    start: 3.5,
			                    end: 5, 
			                    endWidth: 12,
			                    startWidth: 12,
			                    color: '#8ECB60'
			                }],
			                pointers: [{
			                    value: <?=$resultScore;?>
			                }]
			            }]
			        };
			        var circulargauge = new ej.circulargauge.CircularGauge(config);
			        circulargauge.appendTo('#element');
			</script>
     	<?php endif;?>


      </div>
    </div>
  </div>
</div>