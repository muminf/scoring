<?php 
$title = "Чурсози";
include_once("parts/title_header.php");?>
<?php if(!isset($_GET["spr"])):?>
<form action="php/handler.php" method="post">
	<table class="table-bordered" style="background-color: #ffffff" width="100%">
		<tr>
			<th>ID</th>
			<th>Наименование</th>
			<th>Ответы</th>
			<th>Статус</th>
			<th>Сортировка</th>
		</tr>
		<?php foreach($spr as $k=>$v):?>
		<tr>
			<td><?=$k;?></td>
			<td><input class="form-control" name="name[<?=$k;?>]" type="text" value="<?=$v["name"];?>"></td>
			<td><a href="index.php?c=config&spr=<?=$k;?>"><?=count($v["list"]);?>&nbsp;<i class="nav-icon fa fa-pencil"></i></a></td>
			<td><select class="form-control" name="active[<?=$k;?>]"><?=getOptions($status_arr,$v["active"]);?></select></td>
			<td><input class="form-control" name="sort[<?=$k;?>]" type="number" value="<?=$v["sort"];?>"></td>
		</tr>
		<?php endforeach;?>
		<tr>
			<td colspan="5" align="right">
				<a href="index.php?c=newQuestion" class="btn btn-primary">Добавить вопрос</a>
				<input class="btn btn-primary" value="Сохранить" type="submit" name="editSPR">
			</td>
		</tr>
	</table>
</form>
<?php else:?>
	<a href="index.php?c=config"><button class="btn btn-primary"><< Ба кафо</button></a>
	<form action="php/handler.php" method="post">
	<table class="table-bordered" width="100%">
		<tr>
			<th>Справочник</th>
			<th>Ответ</th>
			<th>Балл</th>
			<th>Статус</th>
		</tr>
		<?php foreach($spr[$_GET["spr"]]["list"] as $k=>$v):?>
		<tr>
			<td><select class="form-control" name="id_spr[<?=$k;?>]"><?=getOptions($spr_list,$_GET["spr"]);?></select></td>
			<td><input class="form-control" name="value[<?=$k;?>]" type="text" value="<?=$v;?>"></td>
			<td><input class="form-control" name="score[<?=$k;?>]" type="number" value="<?=$spr[$_GET["spr"]]["score"][$k];?>"></td>
			<td><select class="form-control" name="active[<?=$k;?>]"><?=getOptions($status_arr,$spr[$_GET["spr"]]["active_answer"][$k]);?></select></td>
		</tr>
		<?php endforeach;?>
		<tr>
			<td colspan="5" align="right">
				<input type="hidden" name="curSpr" value="<?=$_GET["spr"];?>">
				<button id="moreAnswer" class="btn btn-warning">Добавить ещё ответ</button>
				<input class="btn btn-primary" value="Сохранить" type="submit" name="editSPRById">
			</td>
		</tr>
	</table>
</form>
<?php endif;?>

<style>
	input[type=number] {
		width:80px;
	}
</style>