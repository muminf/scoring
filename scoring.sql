/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.5.64-MariaDB : Database - scoring
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`scoring` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `scoring`;

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_country` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `id_country` (`id_country`),
  CONSTRAINT `city_ibfk_1` FOREIGN KEY (`id_country`) REFERENCES `country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `city` */

insert  into `city`(`id`,`id_country`,`name`,`active`) values (1,1,'Худжанд',1),(2,1,'Чкаловск',1);

/*Table structure for table `clients` */

DROP TABLE IF EXISTS `clients`;

CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(255) DEFAULT NULL,
  `inn` int(10) DEFAULT NULL,
  `date_born` date DEFAULT NULL,
  `id_city` int(11) DEFAULT NULL,
  `sex` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_city` (`id_city`),
  CONSTRAINT `clients_ibfk_1` FOREIGN KEY (`id_city`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `clients` */

insert  into `clients`(`id`,`fio`,`inn`,`date_born`,`id_city`,`sex`) values (1,'Тест ',534564532,'1991-01-29',1,0),(2,'Абдучабборов Абдучаббор',534564532,'1991-01-29',1,0),(3,'Fуломчон',534564532,'1991-01-29',1,0),(4,'123',534564532,'1991-01-29',1,0),(5,'Дддд',123123123,'1989-02-11',1,1),(6,'Один два три',155200639,'2020-03-07',1,1),(7,'Тест тест',9272728,'2020-10-20',2,0),(8,'Проверка',2147483647,'1999-01-01',1,1),(9,'Абдучаббор',123456789,'2001-01-01',1,0),(10,'Салимов Азимҷон',0,'1998-02-20',1,0),(11,'Як ду се',77188118,'2003-06-09',2,1);

/*Table structure for table `country` */

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `country` */

insert  into `country`(`id`,`name`,`active`) values (1,'Точикистон',1);

/*Table structure for table `phones` */

DROP TABLE IF EXISTS `phones`;

CREATE TABLE `phones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) DEFAULT NULL,
  `phone` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_client` (`id_client`),
  CONSTRAINT `phones_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `phones` */

insert  into `phones`(`id`,`id_client`,`phone`) values (1,1,'123321'),(2,1,'3123123');

/*Table structure for table `result` */

DROP TABLE IF EXISTS `result`;

CREATE TABLE `result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_client` int(11) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `max_score` double DEFAULT NULL,
  `score` double DEFAULT NULL,
  `percent` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_client` (`id_client`),
  CONSTRAINT `result_ibfk_1` FOREIGN KEY (`id_client`) REFERENCES `clients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `result` */

insert  into `result`(`id`,`id_client`,`date_create`,`max_score`,`score`,`percent`) values (1,1,'2020-03-06 09:20:53',125,120,96),(2,1,'2020-03-07 08:23:15',231,147,63.64),(3,4,'2020-03-07 08:26:10',231,147,63.64),(4,3,'2020-03-07 08:03:43',231,147,63.64),(5,6,'2020-03-07 11:12:52',231,162,70.13),(6,3,'2020-03-07 11:17:31',231,147,63.64),(7,3,'2020-03-07 11:18:14',231,147,63.64),(8,3,'2020-03-07 11:18:25',231,147,63.64),(9,3,'2020-03-07 11:18:57',231,147,63.64),(10,3,'2020-03-07 11:19:16',231,147,63.64),(11,3,'2020-03-07 11:40:39',231,147,63.64),(12,3,'2020-03-07 11:40:48',231,147,63.64),(13,3,'2020-03-08 07:06:57',231,147,63.64),(14,4,'2020-03-08 07:07:28',231,147,63.64),(15,4,'2020-03-10 07:04:45',231,147,63.64),(16,8,'2020-03-10 09:16:30',231,156,67.53),(17,4,'2020-03-10 10:07:37',231,156,67.53),(18,8,'2020-03-10 10:31:16',231,153,66.23),(19,3,'2020-03-11 06:16:44',231,147,63.64),(20,5,'2020-04-06 11:28:03',231,147,63.64),(21,5,'2020-04-08 03:35:21',231,147,63.64),(22,1,'2020-04-11 07:15:53',231,140,60.61),(23,11,'2020-04-21 14:54:56',231,219,94.81),(24,2,'2020-04-22 05:30:34',231,201,87.01);

/*Table structure for table `spr_key` */

DROP TABLE IF EXISTS `spr_key`;

CREATE TABLE `spr_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `sort` int(11) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `spr_key` */

insert  into `spr_key`(`id`,`name`,`active`,`sort`,`label`) values (1,'Возраст',1,1,'age'),(2,'Образование',1,2,NULL),(3,'Состоит ли в браке',1,3,NULL),(4,'Брал ли кредит ранее',1,4,NULL),(5,'Трудовой стаж',1,5,NULL),(6,'Наличие автомобиля',1,6,NULL),(9,'Место работы',1,0,NULL);

/*Table structure for table `spr_val` */

DROP TABLE IF EXISTS `spr_val`;

CREATE TABLE `spr_val` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_spr` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `score` double DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `min_val` int(11) DEFAULT NULL,
  `max_val` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_spr` (`id_spr`),
  CONSTRAINT `spr_val_ibfk_1` FOREIGN KEY (`id_spr`) REFERENCES `spr_key` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Data for the table `spr_val` */

insert  into `spr_val`(`id`,`id_spr`,`value`,`score`,`active`,`min_val`,`max_val`) values (1,1,'30-34 года',30,1,30,34),(2,1,'35-50 лет',35,1,35,50),(3,1,'Старше 50 лет',28,1,NULL,NULL),(4,2,'Среднее',22,1,NULL,NULL),(5,2,'Среднее специальное',29,1,NULL,NULL),(6,2,'Высшее',35,1,NULL,NULL),(7,3,'Да',25,1,NULL,NULL),(8,3,'Нет',12,1,NULL,NULL),(9,4,'Да',41,1,NULL,NULL),(10,4,'Нет',22,1,NULL,NULL),(11,5,'Менее 1 года',16,1,NULL,NULL),(12,5,'От 1 до 5 лет',19,1,NULL,NULL),(13,5,'От 5 до 10 лет',24,1,NULL,NULL),(14,5,'Более 10 лет',31,1,NULL,NULL),(15,6,'Да',49,1,NULL,NULL),(16,6,'Нет',18,1,NULL,NULL),(20,1,'До 30 лет',30,1,18,29),(21,9,'Бюдж уч',10,1,NULL,NULL),(22,9,'ЧДММ',15,1,NULL,NULL),(23,9,'ЧП',12,1,NULL,NULL),(24,9,'Другое',5,1,NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
