<?php
include("php/conn.php");
include("php/tools.php");
error_reporting(0);
$pages = array("calc","result","config","newQuestion","client","edit_client","scoring","history");
$spr = getSprArr();
$status_arr = array(
  0 => "Не активен",
  1 => "Активен"
);
$spr_list = array();
foreach($spr as $k=>$v){
  $spr_list[$k] = $v["name"];
}
$clients_list = getClientList(0,0,1);

if(isset($_GET["excel"])){
  toExcel($_GET["excel"]);
}

?>
<?php include_once("parts/header.php");?>
<?php
  if(isset($_GET["c"])){
    if(in_array($_GET["c"], $pages)){
      include("controllers/".$_GET["c"].".php");
    }else{
      include("controllers/dashboard.php");
    }
  }else{
    include("controllers/dashboard.php");
  }
?>
<?php include_once("parts/footer.php");?>        