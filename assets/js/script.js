$("document").ready(function(){
	$("#moreAnswer").click(function(e){
		e.preventDefault();
		$(this).before(`<div class="form-group row">
			<div class="col-md-4">
				<label>Ответ</label>
				<input class="form-control" name="new_answer[]">
			</div>
			<div class="col-md-4">
				<label>Балл</label>
				<input class="form-control" name="new_score[]">
			</div>
		</div>`);
	});
	$('.datepicker').datepicker();
});