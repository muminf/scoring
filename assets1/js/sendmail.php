<?
//require("/home/bitrix/www/bitrix/header.php");
date_default_timezone_set('Asia/Dushanbe');
$i = strftime('%H',time());
echo 'current_time: '.$i;
if($i<7 || $i>20){
	exit;
}
$DB = new PDO('mysql:host=10.10.2.9;dbname=sitemanager','ibs','ibs');
//$DB->Query("insert into crontest(date_time,script) values('".date("Y-m-d H:i:s")."','autoreg')");


$mssql = new PDO('dblib:host=10.10.2.112;dbname=terminals','user1','user1');


/*
$sql = "INSERT INTO crontest(date_time,script)values('".date('Y-m-d H:i:s')."','autoreg')";
mysql_query($sql);
*/
$actSQL = "select * from zayavka_callcenter where status=1";
$q = $DB->Query($actSQL);
$activeArr = array();
while($r = $q->Fetch()){
	$term = intval(substr($r['nomer_terminala'],0,7));
	$activeArr[$term] = array('term'=>$r['nomer_terminala'],'err'=>$r['tip_nepoladki']) ;
}


/*
$serverName = "10.10.2.112"; //serverName\instanceName
$connectionInfo = array( "Database"=>"terminals", "UID"=>"user1", "PWD"=>"user1");
$conn = mssql_connect( $serverName, "user1","user1");
*/



$q = $DB->Query("SELECT code FROM pt_type WHERE code>0 and error=1 and active=1");
$errCode = "";
while($r = $q->Fetch()){
	$errCode .= $r['code'].",";
}
$q = $DB->Query("SELECT max(errorid) as errorid FROM zayavka_callcenter limit 1");
$r = $q->Fetch();
$lastError = ($r['errorid']>0?$r['errorid']:0);
$errCodeStr = substr($errCode,0,strlen($errCode)-1);
$dealers = "1111,500001,400002,300001,7777777";
/*$errSQL = "SELECT  [ErrorID]
      ,[ErrorCode]
      ,[TerminalID]
      ,[EventDateTime]
      ,[DeviceType]
  FROM [Terminals].[dbo].[Errors] 
  WHERE [ErrorCode] in (".$errCodeStr.") AND [ErrorID] > ".$lastError."
  AND [TerminalID] NOT IN(".$dealers.")
  ORDER BY EventDateTime DESC";*/
$errSQL = "SELECT E.ErrorID as ErrorID
      ,E.ErrorCode as ErrorCode
      ,E.TerminalID	as TerminalID
      ,convert(varchar, E.EventDateTime, 120) as EventDateTime
      ,E.DeviceType
  FROM [Terminals].[dbo].[Errors] as E, [Terminals].[dbo].Terminals as T
  WHERE T.TerminalID=E.TerminalID AND
  E.ErrorCode IN (".$errCodeStr.") AND T.Status IN (".$errCodeStr.") AND E.ErrorID>".$lastError."
  ORDER BY E.EventDateTime DESC"; 
  //echo $errSQL."<br><br>";
$respSQL = "SELECT  
       [TerminalID]
      ,[Name]
      ,[LastConnectTime]
  FROM [Terminals].[dbo].[Terminals]
  WHERE DATEDIFF(MINUTE,[LastConnectTime], GETDATE()) > 30
  AND [TerminalID] NOT IN(".$dealers.")"; //echo $respSQL."<br><br>";
  
$paySQL = "SELECT 
       [TerminalID]
      ,[Name]
      ,[LastPaymentTime]
  FROM [Terminals].[dbo].[Terminals]
  WHERE DATEDIFF(MINUTE,[LastPaymentTime], GETDATE()) > 180
  AND [TerminalID] NOT IN(".$dealers.")";
$errArr = array();
//$arr = array();
$respArr = array();
$q = $mssql->query($respSQL);
while($r = $q->Fetch()){
	//$arr[] = $r;
	$respArr[$r['TerminalID']] = 1;
	if(!array_key_exists($r['TerminalID'],$activeArr)){
		$errArr[$r['TerminalID']] = array('term'=>$r['TerminalID'],'err'=>1000,'id'=>0,'time'=>$r['LastConnectTime']);
	}
}
/*
echo "<pre>";
print_r($arr);
echo "</pre>";*/
$q = $mssql->query($errSQL);

$last = 0; //echo $errSQL."<br>";
if($lastError>0){
	while($r = $q->Fetch()){
		if(!array_key_exists($r['TerminalID'],$activeArr) && !array_key_exists($r['TerminalID'],$errArr)){
			if(array_key_exists($r['TerminalID'],$errArr)){
				$last = $r['ErrorID'];
			}else{
				$errArr[$r['TerminalID']] = array('term'=>$r['TerminalID'],'err'=>$r['ErrorCode'],'id'=>$r['ErrorID'],'time'=>$r['EventDateTime']);
			}
		//	echo $r['TerminalID']." ".$r['ErrorCode']."<br>";
		}
	} 
}

if($i>8){
	$q = $mssql->query($paySQL);
	while($r = $q->Fetch()){
		if(!array_key_exists($r['TerminalID'],$activeArr)  && !array_key_exists($r['TerminalID'],$errArr)){
			$sql = "SELECT id 
			FROM zayavka_callcenter 
			WHERE data_zakritie IS NOT NULL
			AND (UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(data_zakritie))<10800
			AND ROUND(nomer_terminala)=".$r['TerminalID']." ORDER BY id DESC LIMIT 1";
			$q = $DB->Query($sql);
			$res = $q->Fetch();
			if(!($res['id']>0)){
				$errArr[$r['TerminalID']] = array('term'=>$r['TerminalID'],'err'=>1001,'id'=>0,'time'=>$r['LastPaymentTime']);
			}
		}
	}
}
/*
echo "<pre>"; 
	print_r($errArr);
echo "</pre>"; */
function getTerminalInfo($code){
	global $DB;
	$sql = "SELECT pt.agent code,	
				pt.name terminal,
		  c.country_name country,
					t.name tehnik 
			FROM pt_terminal pt 
			left join pt_country c on pt.pt_country_id=c.id
			left join pt_tehnik t on pt.pt_tehnik_id=t.id
			WHERE floor(substr(pt.agent,1,7)) = ".$code."
			limit 1";
		//	echo $sql;
	$q = $DB->Query($sql);
	$res = array();
	while($r = $q->Fetch()){
		$res = $r;
	}
	return $res;
}
	

$insertSQL = "INSERT INTO zayavka_callcenter(fio_operatora_open,
											nomer_terminala,
												  data_open,
											  tip_nepoladki,
													  otdel,
												fio_texnika,
											texnik_operator,
													 status,
													  sstat,
													  errorid)";
$errors = array();
$q = $DB->Query("SELECT name,code FROM pt_type WHERE code is not null");
while($r = $q->Fetch()){
	$errors[$r['code']] = $r['name'];
}
$GO = array(927598088=>1,
			927566605=>2,
			928445500=>3,
			927277711=>4,
			927551199=>5,
			934039909=>6,
			888880563=>7,
			888880561=>8,
			888880559=>9,
			888880560=>11,
			987079797=>12,
			927779345=>13,
			929090969=>14);
			
foreach($errArr as $k=>$v){
	$time = date('Y-m-d H:i:s',time());
	$terminal = getTerminalInfo($v['term']);
	$code = ($terminal['code']<>''?$terminal['code']:$v['term']);
	echo $insertSQL = "INSERT IGNORE INTO zayavka_callcenter(fio_operatora_open,
											nomer_terminala,
												  data_open,
											  tip_nepoladki,
													  otdel,
												fio_texnika,
											texnik_operator,
													 status,
													  sstat,
													  errorid,
													  errortime,
													  terminal_id)
											VALUES('Битрикс',
								'".$code.'        '.$terminal['terminal']."',
												 '".date("Y-m-d H:i:s",time())."',
 											 '".$v['err']."',
								  '".$terminal['country']."',
								   '".$terminal['tehnik']."',
												   'система',
														   1,
														   3,
												".$v['id'].",'".$v['time']."',".$code.")";
												  echo $insertSQL."<br><br>";
			if(intval($code)>0){
				if($DB->Query($insertSQL)){									
					$nomer_tel = ($terminal['tehnik']<>""?substr($terminal['tehnik'], -9):'927277711');
					$time1=substr($time, -8);
					$smssend=$terminal['terminal'].', №'.$code.', '.$errors[$v['err']].', '.$v['time'];
					$smssend = str_replace(' ','%20',$smssend);
					$tel='927277711';
					$url = "http://10.10.2.30:9120/s.asp?n=".$nomer_tel."&m=".$smssend; //echo $smssend."<br>";
					//if($terminal['sms']){
					/*if(array_key_exists($nomer_tel,$GO)){
						$ch = fopen($url, 'r');	
					}*/
				//	}
				}
			}
	
}
	if($last){
		echo $sql = "UPDATE zayavka_callcenter SET errorid=".$last." where 1=1 order by errorid desc limit 1";
		$DB->Query($sql);	
	}
	$DB->Query("DELETE FROM zayavka_callcenter WHERE terminal_id=0");
?>