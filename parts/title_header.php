<div class="row page-title-header">
  <div class="col-12">
    <div class="page-header">
      <h4 class="page-title"><?=$title;?></h4>
      <div class="quick-link-wrapper w-100 d-md-flex flex-md-wrap">
        <div class="dropdown ml-lg-auto ml-3 toolbar-item">
          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownexport" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Зеркаши</button>
          <div class="dropdown-menu" aria-labelledby="dropdownexport" x-placement="bottom-start" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 33px, 0px);">
            <a class="dropdown-item" href="<?=SITE_DIR;?>/?excel=result">Хисобот</a>
            <a class="dropdown-item" href="<?=SITE_DIR;?>/?excel=client">Мизочон</a>
          </div>
        </div>
        <ul class="quick-links ml-auto">
          <li><a href="<?=SITE_DIR;?>/?c=config">Чурсози</a></li>
          <li><a href="<?=SITE_DIR;?>/?c=history">Хисобот</a></li>
          <li><a href="<?=SITE_DIR;?>/?c=client">Мизочон</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>