<?php 
  $list = getResultList(0,10);
?>
<div class="col-md-12 grid-margin">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between">
          <h4 class="card-title mb-0">Хисоботи охирин</h4>
          <a href="<?=SITE_DIR;?>/?c=history"><small>Пурра</small></a>
        </div>
        <p>Дар чадвал хисоби скорингии охирин оварда шудааст</p>
        <div class="table-responsive">
          <table class="table table-striped table-hover">
            <thead>
              <tr>
                <th>ID</th>
                <th>ID мизоч</th>
                <th>Ному насаби мизоч</th>
                <th>РМА</th>
                <th>Натича %</th>
                <th>Санаи хисоб</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach($list as $k=>$v):?>
              <tr>
                <td><?=$v["id"];?></td>
                <td><?=$v["cid"];?></td>
                <td><?=$v["fio"];?></td>
                <td><?=$v["inn"];?></td>
                <td><?=$v["percent"];?></td>
                <td><?=$v["date_create"];?></td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>